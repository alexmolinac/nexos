<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210913000850 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE transacciones (id_transacciones INT AUTO_INCREMENT NOT NULL, usuarios_id_usuarios INT DEFAULT NULL, concepto VARCHAR(150) NOT NULL, INDEX fk_transacciones_usuarios1_idx (usuarios_id_usuarios), PRIMARY KEY(id_transacciones)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transacciones ADD CONSTRAINT FK_66C5ED5E236CAE42 FOREIGN KEY (usuarios_id_usuarios) REFERENCES usuarios (id_usuarios)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE transacciones');
    }
}
