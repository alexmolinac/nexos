<?php

/**
 * ApiController.php
 *
 * API Controller
 *
 * @category   Controller
 * @package    MyKanban
 * @author     Francisco Ugalde
 * @copyright  2018 www.franciscougalde.com
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\Usuarios;
use App\Entity\CuentasBancarias;
use App\Entity\TiposCuentas;
use App\Entity\EntidadBancaria;
use App\Entity\Transacciones;

use App\Model\ApiModel;

/**
 * Class ApiController
 *
 * @Route("/api")
 */
class ApiController extends FOSRestController
{

    /**
     * @Rest\Post("/login_check", name="user_login_check")
     *
     */
    public function getLoginCheckAction(Request $request) {

        $entityManager = $this->getDoctrine()->getManager();
        
        $apiModel = new ApiModel();

        $email = strtolower($request->get('email'));
        $password     = $request->get('password');

        $user = $this->getDoctrine()->getRepository(Usuarios::class)
            ->findOneBy(['email' => $email]);

        if (!$user) {
            return new JsonResponse([
                'status'  => 'error',
                'code'    => 404,
                'message' => 'El usuario no se encuentra en la base de datos',
            ]);
        }
        else{
            // Check Password
            if (!$this->get('security.password_encoder')->isPasswordValid($user, $password)) {
                $user = null;
                return new JsonResponse([
                    'status'  => 'error',
                    'code'    => 404,
                    'message' => 'El usuario y/o contraseña son incorrectos',
                ]);
            }

            // Create JWT token
            $token = $this->get('lexik_jwt_authentication.encoder')
                ->encode(['email' => $user->getEmail()]);

            $currentUser = array(
                'id'                => $user->getIdUsuarios(),
                'cedula'            => $user->getCedula(),
                'name'              => $user->getNombres(),
                'surname'           => $user->getApellidos(),
                'email'             => $user->getEmail(),
                'avatar'            => $user->getAvatar(),
                'token'             => $token
            );
        }
        

        return new JsonResponse($currentUser);
    }

    /**
     * @Rest\Post("/register_user", name="user_register")
     *
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder) {
        
        $em = $this->getDoctrine()->getManager();
        
        $email = strtolower($request->get('email'));
        $password = $request->get('password');

        $user = new Usuarios();

        $user_exists = $this->getDoctrine()->getRepository(Usuarios::class)
            ->findOneBy(['email' => $email]);

        $apiModel = new ApiModel();

        $status = false;
        $message = "";

        try {
            
            if( !$user_exists ){
                
                $status = true;
                $message = '¡Felicidades!, ya eres parte de nuestra comunidad';
                
                $user->setCedula($request->get('cedula'));
                $user->setNombres($request->get('nombres'));
                $user->setApellidos($request->get('apellidos'));
                $user->setEmail($request->get('email'));
                $user->setUsername('user'.time());
                $user->setPassword($encoder->encodePassword($user, $password));
                $user->setAvatar($request->get('avatar'));
                $user->setEstado(1);

                $em->persist($user);

                $em->flush();
            }
            else{
                
                $status = false;
                $message = 'Ya existe un usuario creado con la misma cuenta de correo electrónico: ' . $request->get('email');
            }

        } catch (Exception $ex) {
            $status = false;
            $message = "An error has occurred trying to register the user - Error: {$ex->getMessage()}";
        }

        $response = [
            'status'    => $status,
            'message'   => $message,
        ];

        return new JsonResponse($response);
    }

    /**
     * @Rest\Post("/obtener_perfil", name="obtener_perfil")
     */
    public function obtenerPerfil(Request $request){

        $entityManager = $this->getDoctrine()->getManager();
        $apiModel = new ApiModel();

        $user = $apiModel->obtenerPerfil($entityManager, $request->get('usuario_id'));

        return new JsonResponse($user);
    }

    /**
     * @Rest\Post("/edit-user", name="edit-user")
     */
    public function editUserAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(Usuarios::class)
        ->findOneBy(['idUsuarios' => $request->get('id')]);

        $user->setCedula($request->get('cedula'));
        $user->setNombres($request->get('nombres'));
        $user->setApellidos($request->get('apellidos'));
        $user->setEmail($request->get('email'));
        $user->setAvatar($request->get('avatar'));    

        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse([
            'status'  => true,
            'message' => 'Los datos se actualizaron correctamente',
        ]);
    }

    /**
     * @Rest\Post("/change_password", name="change_password")
     *
     */
    public function cambiarPassword(Request $request, UserPasswordEncoderInterface $encoder){

        $status = false;
        $message = "";
        

        $entityManager = $this->getDoctrine()->getManager();

        try {

            $user = $this->getDoctrine()->getRepository(Usuarios::class)->findOneBy(['idUsuarios' => $request->get('id')]);
            $password    = $request->get('password');
            
            $user->setPassword($encoder->encodePassword($user, $password));

            $entityManager->persist($user);
            $entityManager->flush();

            $status = true;
            $message = "Tú contraseña se ha modificado correctamente";

        } catch (Exception $ex) {
            $status = false;
            $message = "Ha ocurrido un error inesperado, intentelo más tarde";
        }



        return new JsonResponse([
            'status'    => $status,
            'message'   => $message
        ]);
    }

    /**
     * @Rest\Get("/obtener_cuentas", name="obtener_cuentas")
     */
    public function obtenerCuentas(Request $request){

        $entityManager = $this->getDoctrine()->getManager();

        $apiModel = new ApiModel();

        $data = $apiModel->obtenerCuentas( $entityManager, $request->get('id') );

        return new JsonResponse( $data );

    }

    /**
     * @Rest\Post("/obtener_mis_cuentas", name="obtener_mis_cuentas")
     */
    public function obtenerMisCuentas(Request $request){

        $entityManager = $this->getDoctrine()->getManager();

        $apiModel = new ApiModel();

        $data = $apiModel->obtenerMisCuentas( $entityManager, $request->get('id_usuario'), $request->get('id_cuenta') );

        return new JsonResponse( $data );

    }

    /**
     * @Rest\Post("/obtener_cuentas_terceros", name="obtener_cuentas_terceros")
     */
    public function obtenerCuentasTerceros(Request $request){

        $entityManager = $this->getDoctrine()->getManager();

        $apiModel = new ApiModel();

        $data = $apiModel->obtenerCuentasTerceros( $entityManager, $request->get('id_usuario'), $request->get('id_cuenta') );

        return new JsonResponse( $data );

    }

    /**
     * @Rest\Post("/inscribir_cuenta", name="inscribir_cuenta")
     */
    public function inscribirCuenta( Request $request ){

        $entityManager = $this->getDoctrine()->getManager();

        $cuenta = new CuentasBancarias;

        $cuenta_existe = $this->getDoctrine()->getRepository(CuentasBancarias::class)->findOneBy(['numero' => $request->get('numero')]);

        if( $cuenta_existe ){
            return new JsonResponse([
                'status'    => false,
                'message'   => 'Ya existe una cuenta con el mismo número de apertura ingresado'
            ]);
        }
        else{

            $banco = $this->getDoctrine()->getRepository(EntidadBancaria::class)->findOneBy(['idEntidadBancaria' => $request->get('entidad_bancaria')]);

            $tipo = $this->getDoctrine()->getRepository(TiposCuentas::class)->findOneBy(['idTiposCuentas' => $request->get('tipo_cuenta')]);

            $user = $this->getDoctrine()->getRepository(Usuarios::class)->findOneBy(['idUsuarios' => $request->get('id_usuario')]);

            $cuenta->setAlias($request->get('alias'));
            $cuenta->setNumero($request->get('numero'));
            $cuenta->setTitular($request->get('titular'));
            $cuenta->setMoneda($request->get('moneda'));
            $cuenta->setSaldo($request->get('saldo'));
            $cuenta->setPropia($request->get('propia'));
            $cuenta->setEstado(1);
            $cuenta->setEntidadBancariaIdEntidadBancaria($banco);
            $cuenta->setTiposCuentasIdTiposCuentas($tipo);
            $cuenta->setUsuariosIdUsuarios($user);

            $entityManager->persist($cuenta);
            $entityManager->flush();

            return new JsonResponse([
                'status'    => true,
                'message'   => 'Se incribio con exito la cuenta'
            ]);
        }
    }

    /**
     * @Rest\Get("/obtener_tipos_cuentas", name="obtener_tipos_cuentas")
     */
    public function obtenerTiposCuentas(){

        $entityManager = $this->getDoctrine()->getManager();
        $apiModel = new ApiModel();

        $data = $apiModel->obtenerTiposCuentas( $entityManager );

        return new JsonResponse( $data );
    }

    /**
     * @Rest\Get("/obtener_entidades_bancarias", name="obtener_entidades_bancarias")
     */
    public function obtenerEntidadesBancarias(){

        $entityManager = $this->getDoctrine()->getManager();
        $apiModel = new ApiModel();

        $data = $apiModel->obtenerEntidadesBancarias( $entityManager );

        return new JsonResponse( $data );
    }

    /**
     * @Rest\Post("/registrar_transaccion", name="registrar_transaccion")
     */
    public function registrarTransaccion( Request $request ){

        $entityManager = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()->getRepository(Usuarios::class)->findOneBy(['idUsuarios' => $request->get('id_usuario')]);

        $cuenta_origen = $this->getDoctrine()->getRepository(CuentasBancarias::class)->findOneBy(['numero' => $request->get('numero_cuenta_origen')]);
        $cuenta_destino = $this->getDoctrine()->getRepository(CuentasBancarias::class)->findOneBy(['numero' => $request->get('numero_cuenta_destino')]);

        if( $cuenta_origen && $cuenta_destino ){

            $cuenta_origen->setSaldo( $request->get('saldo_cuenta_origen') );
            $cuenta_destino->setSaldo( $request->get('saldo_cuenta_destino') );

            $entityManager->persist( $cuenta_origen );
            $entityManager->persist( $cuenta_destino );

            $transaccion = new Transacciones();

            $transaccion->setConcepto( $request->get('concepto') );
            $transaccion->setUsuariosIdUsuarios( $user );

            $entityManager->persist( $transaccion );

            $entityManager->flush();
        }
        else{

            $transaccion = new Transacciones();

            $transaccion->setConcepto( $request->get('concepto') );
            $transaccion->setUsuariosIdUsuarios( $user );

            $entityManager->persist( $transaccion );

            $entityManager->flush();
        }

        return new JsonResponse([
            'status'    => true,
            'message'   => 'La transacción se registró satisfactoriamente'
        ]);
    }

    /**
     * @Rest\Get("/ver_mis_transacciones", name="ver_mis_transacciones")
     */
    public function verMisTransacciones( Request $request ){

        $entityManager = $this->getDoctrine()->getManager();
        $apiModel = new ApiModel();

        $data = $apiModel->verMisTransacciones( $entityManager, $request->get('id_usuario') );

        return new JsonResponse( $data );
    }
}
