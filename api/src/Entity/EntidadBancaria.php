<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntidadBancaria
 *
 * @ORM\Table(name="entidad_bancaria")
 * @ORM\Entity
 */
class EntidadBancaria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_entidad_bancaria", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEntidadBancaria;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=200, nullable=false)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    public function getIdEntidadBancaria(): ?int
    {
        return $this->idEntidadBancaria;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }


}
