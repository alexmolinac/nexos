<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transacciones
 *
 * @ORM\Table(name="transacciones", indexes={@ORM\Index(name="fk_transacciones_usuarios1_idx", columns={"usuarios_id_usuarios"})})
 * @ORM\Entity
 */
class Transacciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_transacciones", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTransacciones;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=150, nullable=false)
     */
    private $concepto;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuarios_id_usuarios", referencedColumnName="id_usuarios")
     * })
     */
    private $usuariosIdUsuarios;

    public function getIdTransacciones(): ?int
    {
        return $this->idTransacciones;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getUsuariosIdUsuarios(): ?Usuarios
    {
        return $this->usuariosIdUsuarios;
    }

    public function setUsuariosIdUsuarios(?Usuarios $usuariosIdUsuarios): self
    {
        $this->usuariosIdUsuarios = $usuariosIdUsuarios;

        return $this;
    }
}