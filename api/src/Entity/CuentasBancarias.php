<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CuentasBancarias
 *
 * @ORM\Table(name="cuentas_bancarias", indexes={@ORM\Index(name="fk_cuentas_bancarias_entidad_bancaria1_idx", columns={"entidad_bancaria_id_entidad_bancaria"}), @ORM\Index(name="fk_cuentas_bancarias_usuarios1_idx", columns={"usuarios_id_usuarios"}), @ORM\Index(name="fk_cuentas_bancarias_tipos_cuentas_idx", columns={"tipos_cuentas_id_tipos_cuentas"})})
 * @ORM\Entity
 */
class CuentasBancarias
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_cuentas_bancarias", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCuentasBancarias;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=30, nullable=false)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=10, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="titular", type="string", length=10, nullable=false)
     */
    private $titular;

    /**
     * @var string
     *
     * @ORM\Column(name="saldo", type="string", length=40, nullable=false)
     */
    private $saldo;

    /**
     * @var string
     *
     * @ORM\Column(name="moneda", type="string", length=3, nullable=false)
     */
    private $moneda;

    /**
     * @var bool
     *
     * @ORM\Column(name="propia", type="boolean", nullable=false)
     */
    private $propia;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var \EntidadBancaria
     *
     * @ORM\ManyToOne(targetEntity="EntidadBancaria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entidad_bancaria_id_entidad_bancaria", referencedColumnName="id_entidad_bancaria")
     * })
     */
    private $entidadBancariaIdEntidadBancaria;

    /**
     * @var \TiposCuentas
     *
     * @ORM\ManyToOne(targetEntity="TiposCuentas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipos_cuentas_id_tipos_cuentas", referencedColumnName="id_tipos_cuentas")
     * })
     */
    private $tiposCuentasIdTiposCuentas;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuarios_id_usuarios", referencedColumnName="id_usuarios")
     * })
     */
    private $usuariosIdUsuarios;

    public function getIdCuentasBancarias(): ?int
    {
        return $this->idCuentasBancarias;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getTitular(): ?string
    {
        return $this->titular;
    }

    public function setTitular(string $titular): self
    {
        $this->titular = $titular;

        return $this;
    }

    public function getSaldo(): ?string
    {
        return $this->saldo;
    }

    public function setSaldo(string $saldo): self
    {
        $this->saldo = $saldo;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getPropia(): ?bool
    {
        return $this->propia;
    }

    public function setPropia(bool $propia): self
    {
        $this->propia = $propia;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEntidadBancariaIdEntidadBancaria(): ?EntidadBancaria
    {
        return $this->entidadBancariaIdEntidadBancaria;
    }

    public function setEntidadBancariaIdEntidadBancaria(?EntidadBancaria $entidadBancariaIdEntidadBancaria): self
    {
        $this->entidadBancariaIdEntidadBancaria = $entidadBancariaIdEntidadBancaria;

        return $this;
    }

    public function getTiposCuentasIdTiposCuentas(): ?TiposCuentas
    {
        return $this->tiposCuentasIdTiposCuentas;
    }

    public function setTiposCuentasIdTiposCuentas(?TiposCuentas $tiposCuentasIdTiposCuentas): self
    {
        $this->tiposCuentasIdTiposCuentas = $tiposCuentasIdTiposCuentas;

        return $this;
    }

    public function getUsuariosIdUsuarios(): ?Usuarios
    {
        return $this->usuariosIdUsuarios;
    }

    public function setUsuariosIdUsuarios(?Usuarios $usuariosIdUsuarios): self
    {
        $this->usuariosIdUsuarios = $usuariosIdUsuarios;

        return $this;
    }


}
