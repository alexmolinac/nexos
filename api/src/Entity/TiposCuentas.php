<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TiposCuentas
 *
 * @ORM\Table(name="tipos_cuentas")
 * @ORM\Entity
 */
class TiposCuentas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_tipos_cuentas", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTiposCuentas;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=120, nullable=false)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    public function getIdTiposCuentas(): ?int
    {
        return $this->idTiposCuentas;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }


}
