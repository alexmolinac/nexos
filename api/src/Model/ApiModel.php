<?php

namespace App\Model;

use Doctrine\ORM\EntityManager;

class ApiModel {

    public function obtenerCuentas($em, $id_usuario){
        $qb = $em->createQueryBuilder()
                ->select('c.idCuentasBancarias, c.alias, c.numero, c.moneda, c.saldo, c.titular, c.propia, t.nombre tipo_cuenta')
                ->from('App:CuentasBancarias', 'c')
                ->innerJoin('App:Usuarios', 'u', 'WITH', 'c.usuariosIdUsuarios = u')
                ->innerJoin('App:TiposCuentas', 't', 'WITH', 'c.tiposCuentasIdTiposCuentas = t')
                ->where('u.idUsuarios = :searchTerm')
                ->andWhere('c.estado = 1')
                ->setParameter('searchTerm', $id_usuario)
                ->getQuery();

        return $qb->getArrayResult();
    }

    public function obtenerTiposCuentas( $em ){
        $qb = $em->createQueryBuilder()
                ->select('t')
                ->from('App:TiposCuentas', 't')
                ->where('t.estado = 1')
                ->getQuery();

        return $qb->getArrayResult();
    }

    public function obtenerEntidadesBancarias( $em ){
        $qb = $em->createQueryBuilder()
                ->select('e')
                ->from('App:EntidadBancaria', 'e')
                ->where('e.estado = 1')
                ->getQuery();

        return $qb->getArrayResult();
    }

    public function obtenerMisCuentas( $em, $id_usuario, $except_cuenta = NULL ){

        $qb = $em->createQueryBuilder()
                ->select('c.idCuentasBancarias, c.alias, c.numero, c.saldo, c.moneda, c.titular, c.propia, t.nombre tipo_cuenta')
                ->from('App:CuentasBancarias', 'c')
                ->innerJoin('App:Usuarios', 'u', 'WITH', 'c.usuariosIdUsuarios = u')
                ->innerJoin('App:TiposCuentas', 't', 'WITH', 'c.tiposCuentasIdTiposCuentas = t')
                ->where('u.idUsuarios = :searchTerm')
                ->andWhere('c.estado = 1')
                ->andWhere('c.propia = 1')
                ->setParameter('searchTerm', $id_usuario);

        if( !is_null( $except_cuenta ) ){
            $qb = $qb->andWhere('c.idCuentasBancarias != :exceptCuenta')
                ->setParameter('exceptCuenta', $except_cuenta);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function obtenerCuentasTerceros( $em, $id_usuario, $except_cuenta = NULL ){

        $qb = $em->createQueryBuilder()
                ->select('c.idCuentasBancarias, c.alias, c.numero, c.saldo, c.moneda, c.titular, c.propia, t.nombre tipo_cuenta')
                ->from('App:CuentasBancarias', 'c')
                ->innerJoin('App:Usuarios', 'u', 'WITH', 'c.usuariosIdUsuarios = u')
                ->innerJoin('App:TiposCuentas', 't', 'WITH', 'c.tiposCuentasIdTiposCuentas = t')
                ->where('u.idUsuarios = :searchTerm')
                ->andWhere('c.estado = 1')
                ->andWhere('c.propia = 0')
                ->setParameter('searchTerm', $id_usuario);

        if( !is_null( $except_cuenta ) ){
            $qb = $qb->andWhere('c.idCuentasBancarias != :exceptCuenta')
                ->setParameter('exceptCuenta', $except_cuenta);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function verMisTransacciones( $em, $id_usuario ){
        $qb = $em->createQueryBuilder()
                ->select('t')
                ->from('App:Transacciones', 't')
                ->innerJoin('App:Usuarios', 'u', 'WITH', 't.usuariosIdUsuarios = u')
                ->where('u.idUsuarios = :searchTerm')
                ->setParameter('searchTerm', $id_usuario);

        return $qb->getQuery()->getArrayResult();
    }
}