import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_helpers/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'page-not-found',
    loadChildren: () => import('./pages/page-not-found/page-not-found.module').then( m => m.PageNotFoundModule )
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'perfil',
    loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'mis-cuentas',
    loadChildren: () => import('./pages/mis-cuentas/mis-cuentas.module').then( m => m.MisCuentasPageModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'mis-cuentas/:id',
    loadChildren: () => import('./pages/mis-cuentas/mis-cuentas.module').then( m => m.MisCuentasPageModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'transfers-to-own-accounts',
    loadChildren: () => import('./pages/transfers-to-own-accounts/transfers-to-own-accounts.module').then( m => m.TransfersToOwnAccountsPageModule)
  },
  {
    path: 'transfers-to-other-accounts',
    loadChildren: () => import('./pages/transfers-to-other-accounts/transfers-to-other-accounts.module').then( m => m.TransfersToOtherAccountsPageModule)
  },
  {
    path: 'transacciones',
    loadChildren: () => import('./pages/transacciones/transacciones.module').then( m => m.TransaccionesPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
