import { Component, NgZone } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AuthService } from './_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [
    './side-menu/styles/side-menu.scss',
    './side-menu/styles/side-menu.shell.scss',
    './side-menu/styles/side-menu.responsive.scss'
  ]
})
export class AppComponent {

  perfil = [
    {
      title: 'Mi perfil',
      url: '/perfil',
      ionicIcon: 'people-outline'
    }
  ]

  accounts = [
    {
      title: 'Ver mis cuentas',
      url: '/mis-cuentas',
      ionicIcon: 'eye-outline'
    },
    {
      title: 'Cuentas de terceros',
      url: '/mis-cuentas/terceros',
      ionicIcon: 'eye-outline'
    }
  ]

  transacciones = [
    {
      title: 'Cuentas propias',
      url: '/transfers-to-own-accounts',
      ionicIcon: 'wallet-outline'
    },
    {
      title: 'Otras cuentas',
      url: '/transfers-to-other-accounts',
      ionicIcon: 'wallet-outline'
    },
    {
      title: 'Mis transacciones',
      url: '/transacciones',
      ionicIcon: 'eye-outline'
    },
    
  ]

 
  textDir = 'ltr';

  // Inject HistoryHelperService in the app.components.ts so its available app-wide
  constructor( private SplashScreen: SplashScreen, public auth: AuthService, private router: Router, private ngZone: NgZone ) {
    this.initializeApp();
  }

  async initializeApp() {
    try {
     await this.SplashScreen.hide();
    } catch (err) {
     console.log('This is normal in a browser', err);
    }
  }

  openPage( url ){
    this.ngZone.run(() => this.router.navigate([url,{skipLocationChange:true, refresh:new Date().getTime() }]) );
  }

  logout(){
    this.auth.logout();
    this.router.navigate(['/']);
  }
}