import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    private http: HttpClient
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  doLogin(email:string, password: string): Observable<any> {
    return this.http.post<any>(`${ environment.apiUrl }/login_check`, { email:email, password:password })
    .pipe(map(response => {
      let user = response;
      if(response.status !== 'error'){
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
      }
      return response;
    }));
  }

  doSignUp( user: any ): Observable<any>{
    return this.http.post<any>(`${ environment.apiUrl }/register_user`, user )
    .pipe( map( response => {
      return response;
    }));
  }

  modificarDatosPerfil(object:any): Observable<any>{
    return this.http.post<any>(`${ environment.apiUrl }/edit-user`, object)
    .pipe(map(response => {
      return response;
    }));
  }

  modificarContrasena(object:any): Observable<any>{
    return this.http.post<any>(`${ environment.apiUrl }/change_password`, object)
    .pipe(map(response => {
      return response;
    }))
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}