import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  isLoading: boolean;
  isAlert: boolean;

  constructor(
    private alert: AlertController,
    private loading: LoadingController,
    private toast: ToastController
  ) { }

  async presentAlert(title: string, message: string) {
    this.isAlert = true;
    const alert = await this.alert.create({
      header: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
  async presentLoading() {
    this.isLoading = true;
    return await this.loading.create({
      message: 'Un momento por favor...',
      spinner: 'crescent',
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async presentToast(message:string) {
    const toast = await this.toast.create({
      message: message,
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }

  async dismissLoading() {
    this.isLoading = false;
    return await this.loading.dismiss().then(() => console.log(''));
  }

  async dismissAlert() {
    return await this.alert.dismiss().then(() => console.log(''));
  }

}