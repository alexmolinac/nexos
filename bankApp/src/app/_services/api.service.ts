import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    constructor( private http: HttpClient ){}

    obtenerCuentas( id: number ): Observable<any>{
        return this.http.get<any>(`${ environment.apiUrl }/obtener_cuentas?id=${ id }`)
        .pipe(map(response => {
            return response;
        }));
    }

    agregarCuenta( object: any ): Observable<any>{
        return this.http.post<any>(`${ environment.apiUrl }/inscribir_cuenta`, object)
        .pipe(map(response => {
            return response;
        }))
    }

    obtenerTiposCuentas(): Observable<any>{
        return this.http.get<any>(` ${ environment.apiUrl }/obtener_tipos_cuentas`)
        .pipe(map(response => {
            return response;
        }));
    }

    obtenerEntidadesBancarias(): Observable<any>{
        return this.http.get<any>(` ${ environment.apiUrl }/obtener_entidades_bancarias`)
        .pipe(map(response => {
            return response;
        }));
    }

    obtenerMisCuentas( object: any ): Observable<any>{
        return this.http.post<any>(`${ environment.apiUrl }/obtener_mis_cuentas`, object)
        .pipe(map(response => {
            return response;
        }))
    }

    obtenerCuentasTerceros( object: any ): Observable<any>{
        return this.http.post<any>(`${ environment.apiUrl }/obtener_cuentas_terceros`, object)
        .pipe(map(response => {
            return response;
        }))
    }

    registrarTransaccion( object: any ): Observable<any>{
        return this.http.post<any>(`${ environment.apiUrl }/registrar_transaccion`, object)
        .pipe(map(response => {
            return response;
        }));
    }

    verMisTransacciones( id:number ): Observable<any>{
        return this.http.get<any>(`${ environment.apiUrl }/ver_mis_transacciones?id_usuario=${ id }`)
        .pipe(map(response => {
            return response;
        }))
    }
}