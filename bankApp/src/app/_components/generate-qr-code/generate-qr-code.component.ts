import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { ModalController } from '@ionic/angular';
import { UtilService } from '../../_services/util.service';
import { NgxQrcodeElementTypes, NgxQRCodeModule } from 'ngx-qrcode2';

@Component({
  selector: 'app-generate-qr-code',
  templateUrl: './generate-qr-code.component.html',
  styleUrls: ['./generate-qr-code.component.scss'],
})
export class GenerateQrCodeComponent implements OnInit, AfterContentChecked {

  cuenta: any;
  elementType = NgxQrcodeElementTypes.CANVAS;

  constructor( private modal: ModalController, private base64ToGallery: Base64ToGallery, private util: UtilService ) { }

  ngOnInit() {
    console.log(this.cuenta)
  }

  ngAfterContentChecked(){
    //console.log(this.cuenta);
  }

  downloadQR(){

    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();

    console.log('data: ', imageData);
    let data = imageData.split(',')[1];
    this.base64ToGallery.base64ToGallery( data,
      { prefix: '_img', mediaScanner: true  }
    ).then( async res => {
      this.util.presentAlert('¡Hecho!', 'El código QR se ha guardado en tú dispositivo');
    }, err => console.log(err)
    );
  }

  dismiss(){
    this.modal.dismiss();
  }

}
