import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ShellModule } from '../shell/shell.module';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { CheckboxWrapperComponent } from './checkbox-wrapper/checkbox-wrapper.component';
import { ShowHidePasswordComponent } from './show-hide-password/show-hide-password.component';
import { CambioContrasenaComponent } from './cambio-contrasena/cambio-contrasena.component';
import { InscribirCuentaComponent } from './inscribir-cuenta/inscribir-cuenta.component';
import { GenerateQrCodeComponent } from './generate-qr-code/generate-qr-code.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShellModule,
    IonicModule,
    NgxQRCodeModule
  ],
  declarations: [
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    CambioContrasenaComponent,
    InscribirCuentaComponent,
    GenerateQrCodeComponent
  ],
  exports: [
    ShellModule,
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    CambioContrasenaComponent,
    InscribirCuentaComponent,
    GenerateQrCodeComponent
  ]
})
export class ComponentsModule {}
