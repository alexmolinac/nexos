import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { UtilService } from '../../_services/util.service';
import { AuthService } from '../../_services/auth.service';
import { ApiService } from '../../_services/api.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-inscribir-cuenta',
  templateUrl: './inscribir-cuenta.component.html',
  styleUrls: ['./inscribir-cuenta.component.scss'],
})
export class InscribirCuentaComponent implements OnInit, AfterContentInit {

  cuentaForm: FormGroup;
  propia: boolean;
  tipos_cuentas: Array<any>;
  entidades_bancarias: Array<any>;

  validation_messages = {
    'alias': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'Este campo solo permite letras.' }
    ],
    'numero': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'El campo debe ser numerico.' },
      { type: 'minlength', message: 'El número de cuenta debe tener mínimo 10 caracteres' },
      { type: 'maxlength', message: 'El número de cuenta debe tener maximo 15 caracteres' }
    ],
    'titular': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'Este campo debe ser numerico.' },
      { type: 'maxlength', message: 'El número de cuenta debe tener maximo 10 caracteres' }
    ],
    'moneda': [
      { type: 'required', message: 'El campo es obligatorio.' },
    ],
    'saldo': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'El campo debe ser numerico.' }
    ],
    'tipo_cuenta': [
      { type: 'required', message: 'El campo es obligatorio.' }
    ],
    'entidad_bancaria': [
      { type: 'required', message: 'El campo es obligatorio.' }
    ]
  };

  constructor( private modal: ModalController, private util: UtilService, private auth: AuthService, private api: ApiService ) {

    this.tipos_cuentas = [];
    this.entidades_bancarias = [];
    
    this.cuentaForm = new FormGroup({
      'alias': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/)
      ])),
      'numero': new FormControl('', Validators.compose(([
        Validators.required,
        Validators.pattern(/^[0-9]+$/),
        Validators.minLength(10),
        Validators.maxLength(15)
      ]))),
      'titular': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[0-9]+$/),
        Validators.maxLength(10)
      ])),
      'moneda': new FormControl('', Validators.compose(([
        Validators.required,
      ]))),
      'saldo': new FormControl('', Validators.compose(([
        Validators.required,
        Validators.pattern(/^[0-9]+$/),
      ]))),
      'tipo_cuenta': new FormControl('', Validators.compose(([
        Validators.required
      ]))),
      'entidad_bancaria': new FormControl('', Validators.compose(([
        Validators.required
      ]))),
    });

  }

  ngOnInit() {
    
    this.util.presentLoading();

    this.api.obtenerTiposCuentas().pipe().subscribe(response => {
      this.tipos_cuentas = response;
      this.api.obtenerEntidadesBancarias().pipe( finalize(() =>{
        this.util.dismissLoading();
      })).subscribe(response => {
        this.entidades_bancarias = response;
      });
    });

    
  }

  ngAfterContentInit() {

    this.cuentaForm.get('titular').patchValue(this.propia ? this.auth.currentUserValue.cedula : '' );
    this.cuentaForm.get('saldo').patchValue(this.propia ? '' : 0 );

  }

  doAddAccount( form ){
    let cuenta = {
      alias: form.alias,
      numero: form.numero,
      titular: form.titular,
      moneda: form.moneda,
      saldo: form.saldo,
      tipo_cuenta: form.tipo_cuenta,
      entidad_bancaria: form.entidad_bancaria,
      propia: this.propia ? 1 : 0,
      id_usuario: this.auth.currentUserValue.id
    }

    this.util.presentLoading();
    this.api.agregarCuenta( cuenta ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {
      if( response.status ){
        this.util.presentAlert('¡Hecho!', response.message);
        this.dismiss();
      }
      else{
        this.util.presentAlert('¡Error!', response.message);
      }
    }, err => {
      this.util.presentAlert('¡Error!', 'Se ha producido un error al inscribir la cuenta');
      this.dismiss();
    });
  }

  dismiss(){
    this.modal.dismiss();
  }
}
