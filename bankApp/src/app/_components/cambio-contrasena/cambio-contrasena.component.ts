import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { PasswordValidator } from '../../_validators/password.validator';
import { UtilService } from '../../_services/util.service';
import { AuthService } from '../../_services/auth.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-cambio-contrasena',
  templateUrl: './cambio-contrasena.component.html',
  styleUrls: [
    './styles/cambio-contrasena.component.scss'
  ],
})
export class CambioContrasenaComponent implements OnInit {

  validationsForm: FormGroup;
  matching_passwords_group: FormGroup;

  validations = {
    'password': [
      { type: 'required', message: 'Se requiere contraseña.' },
      { type: 'pattern', message: 'Su contraseña debe contener por lo menos 8 caracteres, al menos una mayúscula, una minúscula, un número y un caracter especial (opcional).' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Se requiere confirmación de contraseña.' }
    ],
    'matching_passwords': [
      { type: 'areNotEqual', message: 'Contraseña no coincide' }
    ]
  };

  constructor(
    private modalController: ModalController,
    private util: UtilService,
    private auth: AuthService
  ) { }

  ngOnInit() {

    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#$^+=!*()@%&.]*).{7,}$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areNotEqual(formGroup);
    });

    this.validationsForm = new FormGroup({
      'matching_passwords': this.matching_passwords_group
    });
  }

  onSubmit(values) {
    let modificar = {
      id: this.auth.currentUserValue.id,
      password: values.matching_passwords.confirm_password
    }

    this.util.presentLoading();
    this.auth.modificarContrasena(modificar).pipe(finalize(()=>{
      this.util.dismissLoading();
    })).subscribe((response => {
      if(response.status){
        this.util.presentAlert('¡Hecho!', response.message);
        this.dismiss();
      }
      else{
        this.util.dismissLoading();
        this.util.presentAlert('¡Error!', response.message);
      }
    }));
  }

  dismiss(): void {
    this.modalController.dismiss();
  }

}