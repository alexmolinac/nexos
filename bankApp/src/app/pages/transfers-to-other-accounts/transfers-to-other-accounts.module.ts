import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransfersToOtherAccountsPageRoutingModule } from './transfers-to-other-accounts-routing.module';

import { TransfersToOtherAccountsPage } from './transfers-to-other-accounts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransfersToOtherAccountsPageRoutingModule
  ],
  declarations: [TransfersToOtherAccountsPage]
})
export class TransfersToOtherAccountsPageModule {}
