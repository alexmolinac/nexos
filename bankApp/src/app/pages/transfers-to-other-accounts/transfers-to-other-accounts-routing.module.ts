import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransfersToOtherAccountsPage } from './transfers-to-other-accounts.page';

const routes: Routes = [
  {
    path: '',
    component: TransfersToOtherAccountsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransfersToOtherAccountsPageRoutingModule {}
