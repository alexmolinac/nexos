import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { UtilService } from 'src/app/_services/util.service';
import { AuthService } from 'src/app/_services/auth.service';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: [
    './styles/login.page.scss'
  ]
})
export class LoginPage {
  loginForm: FormGroup;

  validation_messages = {
    'email': [
      { type: 'required', message: 'La cuenta de correo electrónico es obligatoria.' },
      { type: 'pattern', message: 'Ingresa una cuenta de correo electrónico valida.' }
    ],
    'password': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'Su contraseña debe contener por lo menos 8 caracteres, al menos una mayúscula, una minúscula, un número y un caracter especial (opcional).' }
    ]
  };

  constructor(
    public router: Router,
    public menu: MenuController,
    public auth: AuthService,
    public util: UtilService
  ) {
    this.loginForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/)
      ])),
      'password': new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#$^+=!*()@%&.]*).{7,}$'),
        Validators.required
      ]))
    });
  }

  // Disable side menu for this page
  ionViewDidEnter(): void {
    if( this.auth.currentUserValue ){
      this.router.navigate(['/mis-cuentas']);
    }
    this.menu.enable(false);
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }

  doLogin( form ): void {
    this.util.presentLoading();
    this.auth.doLogin( form.email, form.password ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe(( response => {
      if( response.status !== 'error' ){
        this.util.presentAlert('¡Hola!', 'Bienvenido a bankApp, será dirigido al area de inicio');
        this.router.navigate(['/mis-cuentas']);
      }
      else{
        this.util.presentAlert('¡Error!', response.message);
      }
    }))
  }
}
