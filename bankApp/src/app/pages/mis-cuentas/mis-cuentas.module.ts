import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MisCuentasPageRoutingModule } from './mis-cuentas-routing.module';

import { MisCuentasPage } from './mis-cuentas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MisCuentasPageRoutingModule
  ],
  declarations: [MisCuentasPage]
})
export class MisCuentasPageModule {}
