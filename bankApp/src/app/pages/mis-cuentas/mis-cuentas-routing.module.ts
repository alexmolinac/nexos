import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MisCuentasPage } from './mis-cuentas.page';

const routes: Routes = [
  {
    path: '',
    component: MisCuentasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MisCuentasPageRoutingModule {}
