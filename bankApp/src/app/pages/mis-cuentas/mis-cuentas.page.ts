import { Component, OnInit, NgZone } from '@angular/core';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { UtilService } from '../../_services/util.service';
import { ApiService } from '../../_services/api.service';
import { AuthService } from '../../_services/auth.service';
import { finalize } from 'rxjs/operators';
import { InscribirCuentaComponent } from '../../_components/inscribir-cuenta/inscribir-cuenta.component';
import { Router, ActivatedRoute } from '@angular/router';
import { GenerateQrCodeComponent } from '../../_components/generate-qr-code/generate-qr-code.component';

@Component({
  selector: 'app-mis-cuentas',
  templateUrl: './mis-cuentas.page.html',
  styleUrls: ['./mis-cuentas.page.scss'],
})
export class MisCuentasPage implements OnInit {

  cuentas: Array<any>;
  cuenta_externa: boolean;

  constructor(
    private util: UtilService,
    private api: ApiService,
    private auth: AuthService,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private router: Router,
    private route: ActivatedRoute,
    private ngZone: NgZone
  ) {
    this.cuentas = [];
    this.cuenta_externa = this.route.snapshot.params.id ? true : false;
  }

  ngOnInit() {
    this.util.presentLoading();
    this.api.obtenerCuentas( this.auth.currentUserValue.id ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {
      this.cuentas = response;
    })
  }

  async inscribirCuenta() {
    const modal = await this.modalController.create({
      component: InscribirCuentaComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        propia: this.cuenta_externa ? 0 : 1
      },
      cssClass: 'inscribir-cuenta'
    });
    modal.onDidDismiss().then(() => {
      this.ngZone.run(() => this.router.navigate(['/mis-cuentas',{skipLocationChange:true, refresh:new Date().getTime() }]) );
    });
    return await modal.present();
  }

  async generateQRCode( cuenta ) {
    const modal = await this.modalController.create({
      component: GenerateQrCodeComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        cuenta: cuenta
      },
      cssClass: 'inscribir-cuenta'
    });
    modal.onDidDismiss().then(() => {
      this.ngZone.run(() => this.router.navigate(['/mis-cuentas',{skipLocationChange:true, refresh:new Date().getTime() }]) );
    });
    return await modal.present();
  }

}
