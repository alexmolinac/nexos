import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { PasswordValidator } from '../../_validators/password.validator';
import { UtilService } from 'src/app/_services/util.service';
import { AuthService } from 'src/app/_services/auth.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: [
    './styles/signup.page.scss'
  ]
})
export class SignupPage {

  signupForm: FormGroup;
  matching_passwords_group: FormGroup;
  avatars: Array<any>;
  avatarSlide: any;

  avatarActual: string;

  validation_messages = {
    'cedula': [
      { type: 'required', message: 'La cedula es obligatoria' },
    ],
    'nombres': [
      { type: 'required', message: 'Los nombres son obligatorios' },
      { type: 'pattern', message: 'Este campo solo permite letras' }
    ],
    'apellidos': [
      { type: 'required', message: 'Los apellidos son obligatorios' },
      { type: 'pattern', message: 'Este campo solo permite letras' }
    ],
    'email': [
      { type: 'required', message: 'La cuenta de correo electrónico es obligatoria.' },
      { type: 'pattern', message: 'Ingresa una cuenta de correo electrónico valida.' }
    ],
    'password': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'Su contraseña debe contener por lo menos 8 caracteres, al menos una mayúscula, una minúscula, un número y un caracter especial (opcional).' }
    ],
    'confirm_password': [
      { type: 'required', message: 'La confirmación de contraseña es obligatoria' }
    ],
    'matching_passwords': [
      { type: 'areNotEqual', message: 'Las contraseñas no coinciden' }
    ]
  };

  constructor(
    public router: Router,
    public menu: MenuController,
    public util: UtilService,
    public auth: AuthService
  ) {

    this.avatarActual = '';
    this.avatarSlide =  {
      slidesPerView: 3.5
    };
    this.avatars = [];

    this.matching_passwords_group = new FormGroup({
      'password': new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#$^+=!*()@%&.]*).{7,}$'),
        Validators.required
      ])),
      'confirm_password': new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areNotEqual(formGroup);
    });

    this.signupForm = new FormGroup({
      'cedula': new FormControl('', Validators.required),
      'nombres': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/)
      ])),
      'apellidos': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/)
      ])),
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'avatar': new FormControl('', Validators.required),
      'matching_passwords': this.matching_passwords_group
    });
  }

  // Disable side menu for this page
  ionViewDidEnter(): void {
    this.menu.enable(false);

    this.avatarActual = 'av-1.png';
    
    this.signupForm.get('avatar').patchValue(this.avatarActual);

    this.avatars = [
      {
        "img": "av-1.png",
        "seleccionado": true
      },
      {
        "img": "av-2.png",
        "seleccionado": false
      },
      {
        "img": "av-3.png",
        "seleccionado": false
      },
      {
        "img": "av-4.png",
        "seleccionado": false
      },
      {
        "img": "av-5.png",
        "seleccionado": false
      },
      {
        "img": "av-6.png",
        "seleccionado": false
      },
      {
        "img": "av-7.png",
        "seleccionado": false
      },
      {
        "img": "av-8.png",
        "seleccionado": false
      }
    ]
    this.avatars.forEach(avatar => avatar.seleccionado = false);
    for (const avatar of this.avatars) {

      if (avatar.img === this.avatarActual) {
        avatar.seleccionado = true;
        break;
      }
    }
  }

  seleccionarAvatar(avatar) {
    console.log(avatar);
    this.avatars.forEach(av => av.seleccionado = false);
    avatar.seleccionado = true;

    this.signupForm.get('avatar').patchValue(avatar.img);

  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }

  doSignup( form ): void {
    let user = {
      cedula: form.cedula,
      nombres: form.nombres,
      apellidos: form.apellidos,
      email: form.email,
      password: form.matching_passwords.password,
      avatar: form.avatar
    };
    this.util.presentLoading();
    this.auth.doSignUp( user ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {
      if( response.status ){
        this.util.presentAlert('¡Hola!', response.message);
        this.router.navigate(['/login']);
      }
      else{
        this.util.presentAlert('¡Error!', response.message);
      }
    })
  }
}
