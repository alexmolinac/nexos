import { Component, OnInit, NgZone } from '@angular/core';
import { UtilService } from '../../_services/util.service';
import { ApiService } from '../../_services/api.service';
import { AuthService } from '../../_services/auth.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-transacciones',
  templateUrl: './transacciones.page.html',
  styleUrls: ['./transacciones.page.scss'],
})
export class TransaccionesPage implements OnInit {

  transacciones: Array<any>;

  constructor(
    private util: UtilService,
    private api: ApiService,
    private auth: AuthService,
    private ngZone: NgZone
  ) {
    this.transacciones = [];
  }

  ngOnInit() {
    this.util.presentLoading();
    this.api.verMisTransacciones( this.auth.currentUserValue.id ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {

      response.forEach(element => {
        
        console.log( element );
        let concepto = JSON.parse( element.concepto );
        console.log( concepto );
        let transaccion = {
          idTransacciones: element.idTransacciones,
          numero_cuenta_origen: concepto[0].numero_cuenta_origen,
          numero_cuenta_destino: concepto[0].numero_cuenta_destino,
          monto: concepto[0].monto
        }

        this.transacciones.push( transaccion );
      });
    })
  }
}
