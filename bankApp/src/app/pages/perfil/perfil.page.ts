import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MenuController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { CambioContrasenaComponent } from '../../_components/cambio-contrasena/cambio-contrasena.component';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilService } from '../../_services/util.service';
import { AuthService } from '../../_services/auth.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit, AfterViewInit {

  perfilForm: FormGroup;
  perfilInfo: any;
  avatars: Array<any>;
  avatarSlide: any;

  avatarActual: string;

  validation_messages = {
    'cedula': [
      { type: 'required', message: 'La cedula es obligatoria' }
    ],
    'nombres': [
      { type: 'required', message: 'Los nombres son obligatorios' },
      { type: 'pattern', message: 'Este campo solo permite letras' }
    ],
    'apellidos': [
      { type: 'required', message: 'Los apellidos son obligatorios' },
      { type: 'pattern', message: 'Este campo solo permite letras' }
    ],
    'email': [
      { type: 'required', message: 'La cuenta de correo electrónico es obligatoria.' },
      { type: 'pattern', message: 'Ingresa una cuenta de correo electrónico valida.' }
    ],
    'password': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'Su contraseña debe contener por lo menos 8 caracteres, al menos una mayúscula, una minúscula, un número y un caracter especial (opcional).' }
    ]
  }

  constructor(
    private menu: MenuController,
    private util: UtilService,
    private auth: AuthService,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private router: Router
  ) {
    this.perfilInfo = {};
    this.avatarActual = '';
    this.avatarSlide =  {
      slidesPerView: 3.5
    };
    this.avatars = [];
  }

  ngOnInit() {

    this.perfilForm = new FormGroup({
      'cedula': new FormControl('', Validators.required),
      'nombres': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/)
      ])),
      'apellidos': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/)
      ])),
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'avatar': new FormControl('', Validators.required),
    });

  }

  ngAfterViewInit(){
    
    this.perfilInfo = this.auth.currentUserValue;

    this.perfilForm.patchValue({
      cedula: this.perfilInfo.cedula,
      nombres: this.perfilInfo.name,
      apellidos: this.perfilInfo.surname,
      email: this.perfilInfo.email,
      avatar: this.perfilInfo.avatar,
    })

    this.avatarActual = this.perfilInfo.avatar;
    
    this.perfilForm.get('avatar').patchValue(this.avatarActual);

    this.avatars = [
      {
        "img": "av-1.png",
        "seleccionado": true
      },
      {
        "img": "av-2.png",
        "seleccionado": false
      },
      {
        "img": "av-3.png",
        "seleccionado": false
      },
      {
        "img": "av-4.png",
        "seleccionado": false
      },
      {
        "img": "av-5.png",
        "seleccionado": false
      },
      {
        "img": "av-6.png",
        "seleccionado": false
      },
      {
        "img": "av-7.png",
        "seleccionado": false
      },
      {
        "img": "av-8.png",
        "seleccionado": false
      }
    ]
    this.avatars.forEach(avatar => avatar.seleccionado = false);
    for (const avatar of this.avatars) {

      if (avatar.img === this.avatarActual) {
        avatar.seleccionado = true;
        break;
      }
    }
  }

  async showPasswordModal() {
    const modal = await this.modalController.create({
      component: CambioContrasenaComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    return await modal.present();
  }

  seleccionarAvatar(avatar) {
    this.avatars.forEach(av => av.seleccionado = false);
    avatar.seleccionado = true;

    this.perfilForm.get('avatar').patchValue(avatar.img);

  }

  ionViewWillEnter() {
    this.menu.enable(true);
  }

  onSubmit(form){
    
    form.id = this.auth.currentUserValue.id;

    this.util.presentLoading();
    this.auth.modificarDatosPerfil( form ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {

      this.auth.currentUserValue.cedula = form.cedula;
      this.auth.currentUserValue.name = form.nombres;
      this.auth.currentUserValue.surname = form.apellidos;
      this.auth.currentUserValue.email = form.email;
      this.auth.currentUserValue.avatar = form.avatar;


      this.auth.currentUserSubject.next(this.auth.currentUserValue);
      localStorage.setItem('currentUser', JSON.stringify(this.auth.currentUserValue));
      
      this.util.presentAlert('¡Listo!', response.message);
      this.router.navigate(['/perfil']);
    });
  }
}
