import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransfersToOwnAccountsPage } from './transfers-to-own-accounts.page';

const routes: Routes = [
  {
    path: '',
    component: TransfersToOwnAccountsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransfersToOwnAccountsPageRoutingModule {}
