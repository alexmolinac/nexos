import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilService } from '..//../_services/util.service';
import { ApiService } from '../../_services/api.service';
import { AuthService } from '../../_services/auth.service';
import { finalize } from 'rxjs/operators';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transfers-to-own-accounts',
  templateUrl: './transfers-to-own-accounts.page.html',
  styleUrls: ['./transfers-to-own-accounts.page.scss'],
})
export class TransfersToOwnAccountsPage implements OnInit {

  @ViewChild(IonSegment) segment: IonSegment;
  segmentModel = "simple";

  simpleForm: FormGroup;
  codeForm: FormGroup;

  cuentas_origen: Array<any>;
  cuentas_destino: Array<any>;

  qrData = 'https://google.com';
  scannedCode = null;
  elementType: 'url' | 'canvas' | 'img' = 'canvas';


  validation_messages = {
    'cuenta_origen': [
      { type: 'required', message: 'El campo es obligatorio.' }
    ],
    'cuenta_destino': [
      { type: 'required', message: 'El campo es obligatorio.' }
    ],
    'monto': [
      { type: 'required', message: 'El campo es obligatorio.' },
      { type: 'pattern', message: 'El campo debe ser numerico.' }
    ]
  };

  constructor(
    private util: UtilService,
    private api: ApiService,
    private auth: AuthService,
    private barcodeScanner: BarcodeScanner,
    private base64ToGallery: Base64ToGallery,
    private router: Router,
    private ngZone: NgZone
  ) {

    this.cuentas_origen = [];
    this.cuentas_destino = [];
  }
  

  ngOnInit() {

    this.simpleForm = new FormGroup({
      'cuenta_origen': new FormControl('', Validators.required),
      'cuenta_destino': new FormControl('', Validators.required),
      'monto': new FormControl('', Validators.required)
    });

    this.codeForm = new FormGroup({
      'cuenta_origen': new FormControl('', Validators.required),
      'cuenta_destino': new FormControl( { value: '', disabled: true }, Validators.required),
      'monto': new FormControl('', Validators.required)
    });

    let cuenta = {
      id_usuario: this.auth.currentUserValue.id,
      id_cuenta: null
    }

    this.util.presentLoading();
    this.api.obtenerMisCuentas( cuenta ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {
      this.cuentas_origen = response
    }, err => {
      this.util.presentAlert('¡Error!', 'Error');
    });

  }

  cuentaDestino( event ){

    let cuenta = {
      id_usuario: this.auth.currentUserValue.id,
      id_cuenta: event.target.value
    }

    this.util.presentLoading();
    this.api.obtenerMisCuentas( cuenta ).pipe( finalize(() => {
      this.util.dismissLoading();
    })).subscribe( response => {

      this.cuentas_destino = response

      if( this.segmentModel === 'codigo' ){
        this.codeForm.get('cuenta_destino').patchValue(this.buscarCuentaPorNumero( this.scannedCode ) ? this.buscarCuentaPorNumero( this.scannedCode ).idCuentasBancarias : '');
      }
    }, err => {
      this.util.presentAlert('¡Error!', 'Error');
    });
  }

  scanCode(){
    this.barcodeScanner.scan().then(
      barcodeData => {
        this.scannedCode = barcodeData.text;
      }
    )
  }
  downloadQR(){
    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();

    console.log('data: ', imageData);
    let data = imageData.split(',')[1];
    this.base64ToGallery.base64ToGallery( data,
      { prefix: '_img', mediaScanner: true  }
    ).then( async res => {
      this.util.presentAlert('¡Hecho!', 'El código QR se ha guardado en tú dispositivo');
    }, err => console.log(err)
    );
  }

  segmentChanged(event) {
    /* if(event.detail.value === 'settings') if(!this.signupForm.valid) this.presentFormularioModal();
    if(event.detail.value === 'notification') this.refreshNotifications(); */
  }



  enviarTransaccion( form ){

    let info_cuenta_origen = this.retornarInfoCuenta( this.cuentas_origen, form.cuenta_origen );
    let info_cuenta_destino = this.retornarInfoCuenta( this.cuentas_destino, form.cuenta_destino );

    if( info_cuenta_origen.saldo >= form.monto ){
      
      info_cuenta_origen.saldo =  parseInt( info_cuenta_origen.saldo ) - parseInt( form.monto );
      info_cuenta_destino.saldo = parseInt( info_cuenta_destino.saldo ) + parseInt( form.monto );

      let transaccion = {
        id_usuario: this.auth.currentUserValue.id,
        numero_cuenta_origen: info_cuenta_origen.numero,
        numero_cuenta_destino: info_cuenta_destino.numero,
        saldo_cuenta_origen: info_cuenta_origen.saldo,
        saldo_cuenta_destino: info_cuenta_destino.saldo,
        concepto: ''
      }

      let concepto = [
        { 
          numero_cuenta_origen: info_cuenta_origen.numero,
          numero_cuenta_destino: info_cuenta_destino.numero,
          monto: form.monto
        }
      ]

      transaccion.concepto = JSON.stringify( concepto );

      this.util.presentLoading();
      this.api.registrarTransaccion( transaccion ).pipe( finalize(() => {
        this.util.dismissLoading();
      })).subscribe( response => {
        if( response.status ){
          this.util.presentAlert('¡Hecho!', response.message);
          this.ngZone.run(() => this.router.navigate(['/mis-cuentas',{skipLocationChange:true, refresh:new Date().getTime() }]) );
        }
      }, err => {
        this.util.presentAlert('¡Error!', 'Ha ocurrido un error inesperado');
      });

    }
    else{
      this.util.presentAlert( '¡Uy!', 'Fondos insuficientes' )
    }
  }

  enviarTransaccionQR( form ){

    let info_cuenta_origen = this.retornarInfoCuenta( this.cuentas_origen, form.cuenta_origen );
    let info_cuenta_destino = this.retornarInfoCuenta( this.cuentas_destino, form.cuenta_destino );

    if( info_cuenta_origen.saldo >= form.monto ){
      
      info_cuenta_origen.saldo =  parseInt( info_cuenta_origen.saldo ) - parseInt( form.monto );
      info_cuenta_destino.saldo = parseInt( info_cuenta_destino.saldo ) + parseInt( form.monto );

      let transaccion = {
        id_usuario: this.auth.currentUserValue.id,
        numero_cuenta_origen: info_cuenta_origen.numero,
        numero_cuenta_destino: info_cuenta_destino.numero,
        saldo_cuenta_origen: info_cuenta_origen.saldo,
        saldo_cuenta_destino: info_cuenta_destino.saldo,
        concepto: ''
      }

      let concepto = [
        { 
          numero_cuenta_origen: info_cuenta_origen.numero,
          numero_cuenta_destino: info_cuenta_destino.numero,
          monto: form.monto
        }
      ]

      transaccion.concepto = JSON.stringify( concepto );

      this.util.presentLoading();
      this.api.registrarTransaccion( transaccion ).pipe( finalize(() => {
        this.util.dismissLoading();
      })).subscribe( response => {
        if( response.status ){
          this.util.presentAlert('¡Hecho!', response.message);
          this.ngZone.run(() => this.router.navigate(['/mis-cuentas',{skipLocationChange:true, refresh:new Date().getTime() }]) );
        }
      }, err => {
        this.util.presentAlert('¡Error!', 'Ha ocurrido un error inesperado');
      });

    }
    else{
      this.util.presentAlert( '¡Uy!', 'Fondos insuficientes' )
    }
  }

  retornarInfoCuenta( cuenta, idCuenta ){

    return cuenta.find( element => element.idCuentasBancarias == idCuenta );

  }

  buscarCuentaPorNumero( numeroCuenta ){
    return this.cuentas_destino.find( element => element.numero == numeroCuenta )
  }

}
