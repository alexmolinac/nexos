import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TransfersToOwnAccountsPageRoutingModule } from './transfers-to-own-accounts-routing.module';
import { TransfersToOwnAccountsPage } from './transfers-to-own-accounts.page';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TransfersToOwnAccountsPageRoutingModule,
    NgxQRCodeModule
  ],
  declarations: [TransfersToOwnAccountsPage]
})
export class TransfersToOwnAccountsPageModule {}
